# Build and run the project
```
# export JAVA_HOME
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
# change the folder students
cd students
# build the project
mvn clean install
# remove output directory if exists
rm -rf out/ 
# run the job where `../results.txt` is the input file and `out` the output directory, which must not exist 
~/hadoop-3.3.1/bin/hadoop jar target/students-1.0.jar com.bigdata.students.App ../results.txt out

```
