from collections import OrderedDict
import random

years = range(1990,2018)
sex = ['m', 'w']
grades = [1.0, 1.3, 1.7, 2.0, 2.3, 2.7, 3.0, 3.3, 3.7, 4.0, 5.0]
day = [ "monday", "tuesday", "wednesday", "thursday", "friday" ]
exam_type = [ "written_exam", "oral_exam", "report", "presentation" ]

mats = {}
for y in years:
    w = []
    j=0
    for g in grades:
        w_g = y/min(years)*1/(j+1)
        w.append(w_g)
        j+=1
    mats[y]=[]
    for i in range(1000):
        mats[y].append(int((y-1990+1)*1e5+random.uniform(1,100)))
        grade=random.choices(grades, weights=w)
        s=mats[y][i]%2
        print(mats[y][i], grade[0], sex[s], y, random.choice(day), random.choice(exam_type), sep=',')

